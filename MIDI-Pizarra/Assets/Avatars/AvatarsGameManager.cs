using UnityEngine;
using UnityEngine.UI;

public class AvatarsGameManager : MonoBehaviour {

	public Button backwards;
	public string backwardsScene;

	public Button[] portraits;

	public RectTransform prompt;
	public Image selectedPortrait;
	public InputField nameField;

	public Button accept;
	public Button cancel;

	public string nextScene;

	private Avatar _selectedAvatar;


	public void Awake(){
		for (int i = 0; i < portraits.Length; i++){
			Button current = portraits[i];
			current.onClick.AddListener( () => {
				_selectedAvatar         = current.gameObject.GetComponent<Avatar>();
				selectedPortrait.sprite = _selectedAvatar.portrait;

				foreach (Button portrait in portraits){
					portrait.gameObject.SetActive(false);
				}


				prompt.gameObject.SetActive(true);
			});
		}


		backwards.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(backwardsScene) );

		cancel.onClick.AddListener( () => {
			prompt.gameObject.SetActive(false);
			
			foreach (Button portrait in portraits){
				portrait.gameObject.SetActive(true);
			}
		} );

		accept.onClick.AddListener( () => {
			SessionManager.Instance.fecha_fin_nombre = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			SessionManager.Instance.SetPlayerInfo(_selectedAvatar.gameObject.name, nameField.text, GameStateManager.Instance.gameTitle);
			UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
		});
	}

	void Start(){
		SessionManager.Instance.fecha_inicio_nombre = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
	}

}
