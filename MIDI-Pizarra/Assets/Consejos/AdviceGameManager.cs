﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class AdviceGameManager : MonoBehaviour {

	/*
	 * =========================================================================================
	 * Unity Interface Variables
	 * =========================================================================================
	 */
	public float matchTime = 60;
	public Question[] questions;

	public Animator beginGameUI;
	public TextMeshProUGUI timerText;
	public TextMeshProUGUI correctTMP;
	public TextMeshProUGUI incorrectTMP;

	public Animator GreenBorderAnimator;
	public Animator RedBorderAnimator;
	public Animator GoodAnimator;
	public Animator BadAnimator;

	public AudioSource goodAudio;
	public AudioSource badAudio;

	public GameObject wellDone;
	public GameObject excellent;
	public GameObject tryAgain;

	public AudioSource wellDoneAudio;
	public AudioSource excellentAudio;
	public AudioSource tryAgainAudio;

	public Animator endGameGUIAnimator;
	public CanvasGroup endGameGUICanvasGroup;
	public Button replayButton; 

	public Button back;
	public string prevScene;

	public Button repeatDirective;
	public AudioSource mainDirectiveAudio;
	private AudioSource _repeatDirectiveAudio;

	public string chapterName;
	public string chapterDescription;
	public string storyName;
	public string gameDescription;
	public string levelName;
	public string levelDescription;
	
	/*
	 * =========================================================================================
	 * Private Members
	 * =========================================================================================
	 */
	private bool _hasGameEnded   = false;
	private bool _hasGameStarted = false;
	private float _timer         = 0.0f;

	private bool _onQuestion     = false;
	private int _currentQuestion = 0;

	private int correctAnswers   = 0;
	private int incorrectAnswers = 0;

	private List<Question> _toAsk = new List<Question>();

	private LevelMetaData _meta;

	/*
	 * =========================================================================================
	 * Unity Interface
	 * =========================================================================================
	 */
	void Awake(){
		_timer         = matchTime;
		timerText.text = _timer.ToString("00");

		for (int i = 0; i < questions.Length; i++){
			questions[i].correct.onClick.AddListener(Correct);
			questions[i].incorrect.onClick.AddListener(Incorrect);
		}

		_toAsk = questions.ToList();
		_toAsk.Sort( (lhs, rhs) => (int) Random.Range(-2.0f, 2.0f));

		replayButton.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene("Consejos") );


		_repeatDirectiveAudio = mainDirectiveAudio;
		repeatDirective.onClick.AddListener( () => _repeatDirectiveAudio.Play() );

		// JSON METADATA PRE-HANDLING
		_meta = new LevelMetaData(SessionManager.Instance.nombre_jugador);
		_meta.nombre_capitulo      = chapterName;
		_meta.descripcion_capitulo = chapterDescription;
		_meta.nombre_historia      = storyName;
		_meta.descripcion_juego    = gameDescription;
		_meta.fecha_inicio         = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		_meta.nombre_nivel 	   = levelName;
		_meta.descripcion_nivel    = levelDescription;

		back.onClick.AddListener( ()      => {
				_meta.estado       = "abandonado";
				_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
				_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				_meta.correctas    = correctAnswers.ToString();
				_meta.incorrectas  = incorrectAnswers.ToString();
				GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));

				UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(prevScene);
			});
	}

	void Start(){
		beginGameUI.SetBool("HasStarted", true);
	}

	void Update(){
		if (_hasGameEnded) return;

		// Cannot begin updating clock unless game has started.
		if (_hasGameStarted == false){
			if (beginGameUI.GetCurrentAnimatorStateInfo(0).IsName("Idle")){
				_hasGameStarted = true;
			} else {
				return;
			}
		}

		// If no questions has been raised, raise one.
		if (_onQuestion == false){
			_toAsk[_currentQuestion].gameObject.SetActive(true);
			_toAsk[_currentQuestion].spawnAnimator.SetBool("HasStarted", true);
			_toAsk[_currentQuestion].directiveAudio.Play();
			_repeatDirectiveAudio = _toAsk[_currentQuestion].directiveAudio;

			if (Random.Range(0.0f, 1.0f) > 0.5f){
				Vector3 position = _toAsk[_currentQuestion].correct.transform.position;
				_toAsk[_currentQuestion].correct.transform.position = _toAsk[_currentQuestion].incorrect.transform.position;
				_toAsk[_currentQuestion].incorrect.transform.position = position;
			}

			_onQuestion = true;
		}

		// Once the previous question has finished fading out, prompt a new one.
		if (_toAsk[_currentQuestion].spawnAnimator.GetCurrentAnimatorStateInfo(0).IsName("Off")){
			_toAsk[_currentQuestion].gameObject.SetActive(false);
			_onQuestion = false;
			_currentQuestion++;
			_repeatDirectiveAudio = mainDirectiveAudio;

			// End-Game System
			if (_currentQuestion == _toAsk.Count()){
				if (correctAnswers == _toAsk.Count()){
					excellent.SetActive(true);
					wellDone.SetActive(false);
					tryAgain.SetActive(false);
					excellentAudio.Play();
				} else if (correctAnswers >= incorrectAnswers){
					wellDone.SetActive(true);
					excellent.SetActive(false);
					tryAgain.SetActive(false);
					wellDoneAudio.Play();
				} else {
					wellDone.SetActive(false);
					excellent.SetActive(false);
					tryAgain.SetActive(true);
					tryAgainAudio.Play();

				}
				endGameGUIAnimator.SetBool("EndGame", true);
				endGameGUICanvasGroup.blocksRaycasts = true;
				_hasGameEnded = true;

				//JSON METADATA CODE
				_meta.estado       = "completado";
				_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
				_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				_meta.correctas    = correctAnswers.ToString();
				_meta.incorrectas  = incorrectAnswers.ToString();
				GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));
			}
		}

		// We reset all feedback
		if (GreenBorderAnimator.GetCurrentAnimatorStateInfo(0).IsName("Stopped")){
			GreenBorderAnimator.SetBool("HasStarted", false);
		}

		if (RedBorderAnimator.GetCurrentAnimatorStateInfo(0).IsName("Stopped")){
			RedBorderAnimator.SetBool("HasStarted", false);
		}

		if (GoodAnimator.GetCurrentAnimatorStateInfo(0).IsName("Stopped")){
			GoodAnimator.SetBool("HasStarted", false);
		}

		if (BadAnimator.GetCurrentAnimatorStateInfo(0).IsName("Stopped")){
			BadAnimator.SetBool("HasStarted", false);
		}

		// Update the clock.
		_timer -= Time.deltaTime;
		timerText.text = _timer.ToString("00");
	}

	private void Correct(){ 
		correctAnswers++;
		correctTMP.text = correctAnswers.ToString();
		_toAsk[_currentQuestion].spawnAnimator.SetBool("HasEnded", true);

		GreenBorderAnimator.transform.position = _toAsk[_currentQuestion].correct.transform.position;
		GreenBorderAnimator.SetBool("HasStarted", true);

		GoodAnimator.transform.position = _toAsk[_currentQuestion].correct.transform.position;
		GoodAnimator.SetBool("HasStarted", true);

		goodAudio.Play();
	}

	private void Incorrect(){
		incorrectAnswers++;
		incorrectTMP.text = incorrectAnswers.ToString();
		_toAsk[_currentQuestion].spawnAnimator.SetBool("HasEnded", true);

		RedBorderAnimator.transform.position = _toAsk[_currentQuestion].incorrect.transform.position;
		RedBorderAnimator.SetBool("HasStarted", true);

		BadAnimator.transform.position  = _toAsk[_currentQuestion].incorrect.transform.position;
		BadAnimator.SetBool("HasStarted", true);

		badAudio.Play();
	}
}
