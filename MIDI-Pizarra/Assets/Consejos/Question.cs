﻿using UnityEngine;
using UnityEngine.UI;

public class Question : MonoBehaviour {
	public Button correct;
	public Button incorrect;
	public Animator spawnAnimator;
	public AudioSource directiveAudio;
}
