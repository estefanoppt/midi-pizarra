using UnityEngine;
using UnityEngine.UI;

public class CreditsManager : MonoBehaviour {

	public Animator creditsAnimator;
	public Button pause;
	public Button play;

	public Button returnButton;
	public string prevScene;

	public Button exitButton;

	void Awake(){
		pause.onClick.AddListener( () => {
				creditsAnimator.speed = 0.0f;
				pause.gameObject.SetActive(false);		
				play.gameObject.SetActive(true);
			});

		play.onClick.AddListener( () => {
				creditsAnimator.speed = 1.0f;
				pause.gameObject.SetActive(true);		
				play.gameObject.SetActive(false);
			});

		returnButton.onClick.AddListener( () => {
					UnityEngine.SceneManagement.SceneManager.LoadScene(prevScene);
					SessionManager.Instance.fecha_fin_creditos = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		});

		exitButton.onClick.AddListener( () => Application.Quit() );
	}

		void Start(){
			SessionManager.Instance.fecha_inicio_creditos = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		}
	}
