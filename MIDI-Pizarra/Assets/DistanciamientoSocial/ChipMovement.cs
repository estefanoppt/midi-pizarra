﻿using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Camera))]
public class ChipMovement : MonoBehaviour {
	
	private bool _isHolding = false;
	private Transform _holdObject;
	private Camera _mainCamera;

	void Awake(){
		_mainCamera = GetComponent<Camera>();
	}

	void Update(){
		Vector3 worldClickPosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
		bool isClicking = Input.GetAxis("Fire1") > 0.75f;

		if (!_isHolding && isClicking){

			int layerMask = LayerMask.NameToLayer("Chip");
			layerMask     = 1 << layerMask;

			RaycastHit[] hits = Physics.RaycastAll(
						worldClickPosition,
						Vector3.forward,
						100.0f,
						layerMask
					);

			if (hits.Length > 0){
				RaycastHit min = hits.Aggregate( (lhs, rhs) => lhs.transform.position.z < rhs.transform.position.z ? lhs : rhs );
				_holdObject    = min.transform;
				_isHolding     = true;
			}
		}

		if (_isHolding && isClicking){
			_holdObject.position = new Vector3(worldClickPosition.x, worldClickPosition.y, 0.5f);
		} else if (_isHolding && !isClicking){
			_holdObject.position = new Vector3(worldClickPosition.x, worldClickPosition.y, 1.0f + 0.05f * worldClickPosition.y);
			_holdObject = null;
			_isHolding  = false;
		}
	}

}
