﻿using UnityEngine;

public class ChipSensor : MonoBehaviour { 

	public int i;
	public int j;

	public DistancingGameManager _board;

	void OnTriggerEnter(Collider other){
		if (other.tag != "Chip") return;
		_board[i, j]++;
	}

	void OnTriggerExit(Collider other){
		if (other.tag != "Chip") return;
		_board[i, j]--;
	}

}
