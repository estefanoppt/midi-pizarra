using UnityEngine;
using UnityEngine.UI;

public class DistancingGameManager : MonoBehaviour {

	/*
	 * =========================================================================================
	 * Unity Interface Variables
	 * =========================================================================================
	 */
	public int N = 3;

	public SpriteRenderer[] happyChips;
	public SpriteRenderer[] sadChips;

	public SpriteRenderer[] correctSquares;
	public SpriteRenderer[] incorrectSquares;

	public SpriteRenderer glow;
	public Color correctColor;
	public Color errorColor;

	public int[] winningPattern;

	public ChipMovement movement;
	public Animator endGameGUIAnimator;
	public Button replayButton;

	public AudioSource excellentAudio;

	public float waitForEndGameRegister = 1.0f;

	public Button back;
	public string prevScene;

	public Button repeatDirective;
	public AudioSource repeatDirectiveAudio;

	public string chapterName;
	public string chapterDescription;
	public string storyName;
	public string gameDescription;
	public string levelName;
	public string levelDescription;

	private LevelMetaData _meta;

	/*
	 * =========================================================================================
	 * Public Interface
	 * =========================================================================================
	 */

	private int[,] _M;
	public int this[int i, int j] {
		get {
			if (i < 0 || j < 0 || i >= N || j >= N) return 0;
			else return _M[i, j];
		}

		set {
			_M[i, j]            = value;
			bool isBoardCorrect = CheckBoardCorrectness();
			SetFeedback(isBoardCorrect);

			if (isBoardCorrect && AttainedVictory()){
				_endGame = true;
				_timer   = 0.0f;
			} else {
				_endGame = false;
			}
		}
	}


	/*
	 * =========================================================================================
	 * Private Interface
	 * =========================================================================================
	 */

	/*
	 * This algorithm checks that the board is in the correct state. 
	 */
	private bool CheckBoardCorrectness(){

		// First we check that there are no repeated chips on the same spot.
		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++){
				if (this[i, j] > 1) return false;
			}
		}

		// We then check that no neighbors are present. 
		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++){
				if (this[i, j] == 1){
					bool hasNeighbor = false;
					hasNeighbor = hasNeighbor || this[i - 1, j - 1] == 1;
					hasNeighbor = hasNeighbor || this[i - 1, j    ] == 1;
					hasNeighbor = hasNeighbor || this[i - 1, j + 1] == 1;
					hasNeighbor = hasNeighbor || this[i    , j - 1] == 1;
					hasNeighbor = hasNeighbor || this[i    , j + 1] == 1;
					hasNeighbor = hasNeighbor || this[i + 1, j - 1] == 1;
					hasNeighbor = hasNeighbor || this[i + 1, j    ] == 1;
					hasNeighbor = hasNeighbor || this[i + 1, j + 1] == 1;
				
					if (hasNeighbor) return false;	
				}
			}
		}

		return true;
	}

	/*
	 * Check if board is empty.
	 */
	private bool IsBoardEmpty(){
		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++){
				if (this[i, j] > 0) return false;
			}
		}
		return true;
	}

	/*
	 * This algorithm sets the appropriate feedback given the current board change.
	 */
	private void SetFeedback(bool isBoardCorrect){
		float alpha = (isBoardCorrect ? 1.0f : 0.0f);

		if (!IsBoardEmpty()){
			if (isBoardCorrect){
				glow.color = correctColor;
			} else {
				glow.color = errorColor;
			}
		} else {
			glow.color = new Color(1, 1, 1, 0);
		}

		// Set set happy/sad faces according to correctness of board.
		for (int i = 0; i < happyChips.Length; i++){
			Color hc = happyChips[i].color;
			happyChips[i].color = new Color(hc.r, hc.g, hc.b, alpha);

			Color sc = sadChips[i].color;
			sadChips[i].color   = new Color(sc.r, sc.g, sc.b, 1.0f - alpha);
		}

		// We reset the feedback of the correct/incorrect squares. 
		for (int i = 0; i < correctSquares.Length; i++){
			Color cc = correctSquares[i].color;
			correctSquares[i].color = new Color(cc.r, cc.g, cc.b, 0.0f);

			Color ic = incorrectSquares[i].color;
			incorrectSquares[i].color = new Color(ic.r, ic.g, ic.b, 0.0f);
		}

		// We set appropriate feedback for correct/incorrect squares. 
		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++){
				if (this[i, j] > 0){
					int index = i*N + j;
					Color cc = correctSquares[index].color;
					correctSquares[index].color = new Color(cc.r, cc.g, cc.b, alpha);

					Color ic = incorrectSquares[index].color;
					incorrectSquares[index].color = new Color(ic.r, ic.g, ic.b, 1.0f - alpha);
				}
			}
		}
	}

	/*
	 * Check if player is victorious.
	 */
	private bool AttainedVictory(){

		bool victory = true;

		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++){
				victory = victory && this[i, j] == winningPattern[i*N + j];
			}
		}

		return victory;
	}
	/*
	 * =========================================================================================
	 * Private Members
	 * =========================================================================================
	 */
	private bool _endGame = false;
	private float _timer  = 0.0f;

	/*
	 * =========================================================================================
	 * Unity Interface
	 * =========================================================================================
	 */

	void Awake(){
		Debug.Assert(happyChips.Length == sadChips.Length, "Happy chips don't match sad chips.");
		Debug.Assert(correctSquares.Length == N*N, "Correct squares don't match size of board.");
		Debug.Assert(incorrectSquares.Length == N*N, "Incorrect squares don't match size of board.");

	       	_M = new int[N, N];

		replayButton.onClick.AddListener(() => UnityEngine.SceneManagement.SceneManager.LoadScene("DistanciamientoSocial") );
		repeatDirective.onClick.AddListener( () => repeatDirectiveAudio.Play() );
		
		// JSON METADATA PRE-HANDLING
		_meta = new LevelMetaData(SessionManager.Instance.nombre_jugador);
		_meta.nombre_capitulo      = chapterName;
		_meta.descripcion_capitulo = chapterDescription;
		_meta.nombre_historia      = storyName;
		_meta.descripcion_juego    = gameDescription;
		_meta.fecha_inicio         = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		_meta.nombre_nivel 	   = levelName;
		_meta.descripcion_nivel    = levelDescription;

		back.onClick.AddListener( () => {
					_meta.estado       = "abandonado";
					_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
					_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					_meta.correctas    = (CheckBoardCorrectness() ? 1 : 0 ).ToString();
					_meta.incorrectas  = (!CheckBoardCorrectness() ? 1 : 0).ToString();
					GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));
				
					UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(prevScene);
				});
	}

	void Update(){
		if (!_endGame) return;

		_timer += Time.deltaTime;
		if (_timer > waitForEndGameRegister){
			excellentAudio.Play();
			endGameGUIAnimator.gameObject.SetActive(true);
			endGameGUIAnimator.SetBool("EndGame", true);
			movement.enabled = false;
			this.enabled = false;

			// JSON METADATA POSTHANDLING
			_meta.estado       = "completado";
			_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
			_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			_meta.correctas    = "1";
			_meta.incorrectas  = "0";
			GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));
		}

	}

}
