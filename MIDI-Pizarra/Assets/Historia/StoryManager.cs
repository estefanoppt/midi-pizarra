using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

public class StoryManager : MonoBehaviour {

	public VideoPlayer player;

	public Button pause;
	public Button play;
	public Button replay;
	public Button backwards;

	public string endVideoScene;

	public string chapterName;
	public string chapterDescription;
	public string storyName;
	public string storyDescription;
	public string gameDescription;
	public string videoDuration;
		
	private string _startDate;
	private StoryMetaData _meta;

	void Awake(){
		pause.onClick.AddListener( () => {
					player.Pause();
					pause.gameObject.SetActive(false);
					play.gameObject.SetActive(true);
				});

		play.onClick.AddListener( () => {
					player.Play();
					pause.gameObject.SetActive(true);
					play.gameObject.SetActive(false);
				});

		replay.onClick.AddListener( () => { 
					player.Stop(); 
					player.Play(); 
					pause.gameObject.SetActive(true);
					play.gameObject.SetActive(false);
				} );


		// JSON METADATA HANDLING ------------------------------------------------------
		_meta = new StoryMetaData(SessionManager.Instance.nombre_jugador);
		_meta.nombre_capitulo      = chapterName;
		_meta.descripcion_capitulo = chapterDescription;
		_meta.nombre_historia      = storyName;
		_meta.descripcion_historia = storyDescription;
		_meta.descripcion_juego    = gameDescription;
		_meta.duracion             = videoDuration;
		_meta.fecha_inicio         = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

		player.loopPointReached += (vp) => {
			vp.Stop();
			
			_meta.estado 		  = "completado";
			_meta.tiempo_juego         = Time.timeSinceLevelLoad.ToString("0");
			_meta.fecha_fin            = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));

			UnityEngine.SceneManagement.SceneManager.LoadScene(endVideoScene);
		};

		backwards.onClick.AddListener( () => {
			_meta.estado       = "abandonado";
			_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
			_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));
			
			UnityEngine.SceneManagement.SceneManager.LoadScene(endVideoScene);
		});
	}

}
