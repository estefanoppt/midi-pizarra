﻿using UnityEngine;
using UnityEngine.UI;

public class StoriesMenuManager : MonoBehaviour {
	public Button chapterIButton;
	public string chapterIScene;

	public Button chapterIIButton;
	public string chapterIIScene;

	public Button chapterIIIButton;
	public string chapterIIIScene;

	public Button returnButton;
	public string prevScene;

	public Button toCredits;
	public string creditsScene;

	void Awake(){
		chapterIButton.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(chapterIScene) );
		chapterIIButton.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(chapterIIScene) );
		chapterIIIButton.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(chapterIIIScene) );
		returnButton.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(prevScene) );

		
		toCredits.onClick.AddListener( () => {
				UnityEngine.SceneManagement.SceneManager.LoadScene(creditsScene);
			});
	}
}
