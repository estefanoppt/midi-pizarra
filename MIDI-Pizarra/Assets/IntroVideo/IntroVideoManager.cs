using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

public class IntroVideoManager : MonoBehaviour {
	public VideoPlayer player;
	public long frameToAppear = 1112;

	public Button yes;
	public Button no;

	public Button pause;
	public Button play;
	public Button replay;
	public Button backwards;
	public Button skip;

	public string endVideoScene;
	public string prevScene;

	private bool _isPaused = false;
	void Awake(){

		yes.onClick.AddListener( () => {
				UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
				});

		no.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(endVideoScene) );
		
		pause.onClick.AddListener( () => {
					player.Pause();
					pause.gameObject.SetActive(false);
					play.gameObject.SetActive(true);
					_isPaused = true;
				});

		play.onClick.AddListener( () => {
					player.Play();
					pause.gameObject.SetActive(true);
					play.gameObject.SetActive(false);
					_isPaused = false;
				});

		replay.onClick.AddListener( () => { 
					player.Stop(); 
					player.Play(); 
					pause.gameObject.SetActive(true);
					play.gameObject.SetActive(false);
				} );

		player.loopPointReached += ((vp) => {
		});

		backwards.onClick.AddListener( () => {
			UnityEngine.SceneManagement.SceneManager.LoadScene(prevScene);
		});

		skip.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(endVideoScene) );
	}

	void Start(){
		SessionManager.Instance.fecha_inicio_saludo = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
	}

	void Update(){
		Debug.Log(player.frame + ", " + player.frameCount);
		if (player.frame == frameToAppear){
			player.Pause();
			yes.gameObject.SetActive(true);
			no.gameObject.SetActive(true);
			pause.gameObject.SetActive(false);
			play.gameObject.SetActive(false);

		}
	}
}
