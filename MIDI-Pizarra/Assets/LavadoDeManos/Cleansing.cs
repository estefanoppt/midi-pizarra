using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
 * This simple script interacts with viruses and decreases their health 
 * according the the amount of movement of the cursor during contact 
 * of the Soal and the Virus. 
 */
public class Cleansing : MonoBehaviour {

	/*
	 * =========================================================================================
	 * Unity Interface Variables
	 * =========================================================================================
	 */
	public float cleansingRate = 1.0f;

	/*
	 * =========================================================================================
	 * Private Variables
	 * =========================================================================================
	 */
	private HashSet<Virus> _contacts = new HashSet<Virus>();
	private Vector3 previousPosition = Vector3.zero;

	/*
	 * =========================================================================================
	 * Unity Interface Methods
	 * =========================================================================================
	 */
	void Awake(){
		previousPosition = transform.position;
	}

	/*
	 * On Update() we remove health from all viruses we're in contact with.
	 */
	void FixedUpdate(){
		Vector3 currentPosition = transform.position;
		float delta = Vector3.Magnitude(currentPosition - previousPosition);
		foreach (Virus virus in _contacts.ToList()){
			virus.Health -= cleansingRate * delta;	
		}

		previousPosition = currentPosition;
	}

	/*
	 * In these methods we add and remove viruses as we come in 
	 * contact with them.
	 */
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag != "Virus") return;
		Virus virus = other.GetComponent<Virus>();
		_contacts.Add(virus);
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.tag != "Virus") return;
		Virus virus = other.GetComponent<Virus>();
		_contacts.Remove(virus);
	}

}
