﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CleansingGameManager : MonoBehaviour {
	/*
	 * =========================================================================================
	 * Unity Interface Variables
	 * =========================================================================================
	 */

	/*
	 * The prefab for the Virus object. It must contain
	 * a Virus.cs component. 
	 */
	public GameObject virusPrefab;

	/*
	 * Amount of duration of the game. 
	 */
	public float gameDuration = 20.0f;

	/*
	 * The range where the Viruses will be spawned.
	 */
	public Transform max;
	public Transform min;

	/*
	 * The transform where the viruses will be spawned into.
	 */
	public Transform virusParent;

	/*
	 * The progress bar.
	 */
	public ProgressBar progressBar;

	/*
	 * Canvas animator.
	 */
	public Animator canvasAnimator;

	/*
	 * Cleansing end-game feedback.
	 */
	public Animator endGameGUIAnimator;

	/*
	 * Soap end-game feedback.
	 */
	public Animator endGameSoapAnimator;

	/*
	 * Animator for hand washing.
	 */
	public Animator endGameHandAnimator;

	/*
	 * Button for repeating gameplay.
	 */
	public Button repeatButton;

	public AudioSource excellentAudio;
	public AudioSource wellDoneAudio;
	public AudioSource tryAgainAudio;

	public Button repeatDirective;
	public AudioSource repeatDirectiveAudio;

	/*
	 * Text object for kill count.
	 */
	public TextMeshProUGUI virusDeathText;

	public RectTransform wellDone;
	public RectTransform excellent;
	public RectTransform tryAgain;

	public SpriteRenderer shine;
	public Color failureShineColor;

	public ParticleSystem shineParticles;

	/*
	 * Reward Parameters
	 */
	public int limitFailure = 10;
	public int limitWellDone = 50;

	public Button back;
	public string prevScene;

	public string chapterName;
	public string chapterDescription;
	public string storyName;
	public string gameDescription;
	public string levelName;
	public string levelDescription;

	private LevelMetaData _meta;

	/*
	 * =========================================================================================
	 * Private Variables
	 * =========================================================================================
	 */
	private int _n                         = 1;
	private bool _hasGameStarted           = false;
	private bool _onStage                  = false;
	private bool _playingeEndGameAnimation = false;
	private bool _endGame                  = false;
	private int _numDead                   = 0;
	private int _totalNumDead              = 0;
	private List<Virus> _spawn             = new List<Virus>();

	/*
	 * =========================================================================================
	 * Unity Interface
	 * =========================================================================================
	 */
	private float _timer = 0.0f;
	void Awake(){
		repeatButton.onClick.AddListener(RepeatGame);
		_timer                = gameDuration;
		progressBar.TotalTime = gameDuration;
		shineParticles.Stop();

		repeatDirective.onClick.AddListener( () => repeatDirectiveAudio.Play() );
		
		// JSON METADATA PRE-HANDLING
		_meta = new LevelMetaData(SessionManager.Instance.nombre_jugador);
		_meta.nombre_capitulo      = chapterName;
		_meta.descripcion_capitulo = chapterDescription;
		_meta.nombre_historia      = storyName;
		_meta.descripcion_juego    = gameDescription;
		_meta.fecha_inicio         = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		_meta.nombre_nivel 	   = levelName;
		_meta.descripcion_nivel    = levelDescription;

		back.onClick.AddListener( () => {
					_meta.estado       = "abandonado";
					_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
					_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					_meta.correctas    = _totalNumDead.ToString();
					_meta.incorrectas  = "0";
					GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));
				
					UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(prevScene);
				});
	}

	void Update(){
		if (_endGame) return;

		if (canvasAnimator.GetNextAnimatorStateInfo(0).IsName("CanvasAppeared")){
			_hasGameStarted = true;
		}

		if (!_hasGameStarted) return;

		if (_playingeEndGameAnimation){
			if (endGameHandAnimator.GetCurrentAnimatorStateInfo(0).IsName("Cleaned")){
				_endGame = true;
				endGameGUIAnimator.gameObject.SetActive(true);
				endGameGUIAnimator.SetBool("EndGame", true);
			}
		       	return;
		}

		_timer -= Time.deltaTime;
		if (_timer <= 0.0f) {
			if (_totalNumDead >= 0 && _totalNumDead < limitFailure){
				excellent.gameObject.SetActive(false);
				wellDone.gameObject.SetActive(false);
				tryAgain.gameObject.SetActive(true);
				tryAgainAudio.Play();
				shine.color = failureShineColor;
			} else if (_totalNumDead >= limitFailure && _totalNumDead < limitWellDone){
				excellent.gameObject.SetActive(false);
				wellDone.gameObject.SetActive(true);
				tryAgain.gameObject.SetActive(false);
				wellDoneAudio.Play();
			} else if (_totalNumDead > limitWellDone){
				excellent.gameObject.SetActive(true);
				wellDone.gameObject.SetActive(false);
				tryAgain.gameObject.SetActive(false);
				excellentAudio.Play();
				shineParticles.Play();
			}

			endGameSoapAnimator.SetBool("EndGame", true);
			endGameHandAnimator.SetBool("EndGame", true);
			_playingeEndGameAnimation = true;
			ClearViruses();

			// JSON METADATA POSTTINKERING
			_meta.estado       = "completado";
			_meta.tiempo_juego = Time.timeSinceLevelLoad.ToString("0");
			_meta.fecha_fin    = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
			_meta.correctas    = _numDead.ToString();
			_meta.incorrectas  = "0";
			GameStateManager.Instance.AddJsonToList(JsonUtility.ToJson(_meta));
		}

		progressBar.Timer = _timer;

		if (_onStage == false){
			_onStage = true;
			for (int i = 0; i < _n; i++){
				float x = Random.Range(max.position.x, min.position.x);
				float y = Random.Range(min.position.y, max.position.y);
				float z = virusPrefab.transform.position.z;

				GameObject spawn = Instantiate(virusPrefab, new Vector3(x, y, z), Quaternion.identity);
				spawn.transform.SetParent(virusParent);

				Virus virus = spawn.GetComponent<Virus>();
				virus.OnDeath += (v) => { _numDead++; _totalNumDead++; }; 
				_spawn.Add(virus);
			}
		}

		if (_numDead == _n){
			_onStage = false;
			_numDead = 0;
			_n++;
			ClearViruses();
		}

		virusDeathText.text = "x" + _totalNumDead.ToString();
	}

	private void ClearViruses(){
		foreach (Virus virus in _spawn){
			virus.PhaseOut();
		}
		_spawn.Clear();

	}

	/*
	 * Repeat game event handler.
	 */
	void RepeatGame(){
		UnityEngine.SceneManagement.SceneManager.LoadScene("LavadoDeManos");
	}
}
