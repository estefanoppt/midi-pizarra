using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProgressBar : MonoBehaviour {
	/*
	 * =========================================================================================
	 * Unity Interface
	 * =========================================================================================
	 */
	public float alphaAmp = 1.0f;
	public float flushAmp = 1.0f;

	public TextMeshProUGUI scoreText;

	public RectTransform flush;
	public RectTransform flushStart;
	public RectTransform flushEnd;

	public Image[] chips;


	/*
	 * =========================================================================================
	 * Public Interface
	 * =========================================================================================
	 */
	public float Timer { get; set; }
	public float TotalTime { get; set; }

	/*
	 * =========================================================================================
	 * Private Members
	 * =========================================================================================
	 */
	private int _currentChip = 1;
	private bool _graphing = false;

	/*
	 * =========================================================================================
	 * Unity Interfac
	 * =========================================================================================
	 */
	void Awake(){
		for (int i = 1; i < chips.Length; i++){
			Color c = chips[i].color;
			chips[i].color = new Color(c.r, c.g, c.b, 0.0f);
		}

		flush.position = flushStart.position;
	}

	void Update(){
		float ellapsedTime = TotalTime - Timer;
		float t            = ellapsedTime / TotalTime;

		if (t >= 1.0f) return; 

		flush.position     = Vector3.Lerp(flushStart.position, flushEnd.position, t);;

		if ( flush.position.y >= chips[_currentChip].transform.position.y){
			Color prevCol = chips[_currentChip].color;
			chips[_currentChip].color = new Color(prevCol.r, prevCol.g, prevCol.b, 1.0f);
			_currentChip++;
			_graphing = false;
		}

		scoreText.text = Timer.ToString("00.0");
	}
}
