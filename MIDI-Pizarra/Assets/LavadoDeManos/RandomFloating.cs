using UnityEngine;

public class RandomFloating : MonoBehaviour {

	public float maxAcceleration;
	public float maxRadius = 0.125f;
	public AnimationCurve slerpCurve;

	private Vector3 _newTarget;
	private Vector3 _oldPosition;
	private float   _t = 0.0f;
	private bool    _isLerping = false;

	void Awake(){
		_oldPosition = transform.position;
	}

	void Update(){

		if (_isLerping == false){
			_newTarget = Random.Range(0.0f, maxRadius) * Random.insideUnitCircle;
			_t         = 0.0f;
			_isLerping = true;
		}

		_t += maxAcceleration * slerpCurve.Evaluate(_t) * Time.deltaTime;

		transform.position = _oldPosition +  _t * _newTarget;

		if (_t >= 1.0f){
			_isLerping = false;
			_oldPosition += _newTarget;
		}

	}
}
