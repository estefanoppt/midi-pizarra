using UnityEngine;

public class RockingMotion : MonoBehaviour {

	public float maxTheta        = 1.0f;
	public float maxAcceleration = 45.0f;
	public AnimationCurve rockingCurve;

	private bool _isRocking    = false;
	private float _prevTheta   = 0.0f;
	private float _targetTheta = 0.0f;
	private float _theta       = 0.0f;

	void Update(){
		if (_isRocking == false){
			_isRocking = true;
			_theta     = 0.0f;
			_targetTheta = Random.Range(-maxTheta, maxTheta);
		}

		_theta += maxAcceleration * Mathf.Sign(_targetTheta) * rockingCurve.Evaluate(Mathf.Abs(_theta) / Mathf.Abs(_targetTheta)) * Time.deltaTime;

		Quaternion rotation = Quaternion.Euler(0, 0, _prevTheta + _theta);
		transform.rotation  = rotation;

		if (Mathf.Abs(_theta) > Mathf.Abs(_targetTheta)){
			_isRocking = false;
			_prevTheta += _theta;
		}
	}

}
