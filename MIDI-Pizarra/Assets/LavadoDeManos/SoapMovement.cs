﻿using UnityEngine;

public class SoapMovement : MonoBehaviour {

	public Camera mainCamera;

	void Update(){
		if (Input.GetAxis("Fire1") >= 0.75f){
			Vector3 worldClickPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
			Vector3 worldCoordinates = new Vector3(worldClickPosition.x, worldClickPosition.y, transform.position.z);
			transform.Translate(worldCoordinates - transform.position);
		}
	}


}
