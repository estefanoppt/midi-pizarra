using UnityEngine;

public class SpawnEffect : MonoBehaviour {

	public float startScale = 0.0f;
	public float endScale   = 0.5f;
	public float maxAcceleration = 1.0f;
	public AnimationCurve scalingCurve;

	private float _scale = 0.0f;

	void Update(){
		_scale += maxAcceleration * scalingCurve.Evaluate(_scale/endScale) * Time.deltaTime;
		transform.localScale = new Vector3(_scale, _scale, _scale);
		if (_scale > endScale) enabled = false;
	}
}
