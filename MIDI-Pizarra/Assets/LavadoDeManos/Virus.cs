﻿using UnityEngine;

public delegate void OnVirusDeath(Virus virus);

[RequireComponent(typeof(SpriteRenderer))]
public class Virus : MonoBehaviour {

	/*
	 * =========================================================================================
	 * Unity Interface
	 * =========================================================================================
	 */
	public AnimationCurve healthToAlpha;
	public float phaseOutSpeed = 2.0f;

	/*
	 * =========================================================================================
	 * Public Interface
	 * =========================================================================================
	 */
	public event OnVirusDeath OnDeath;

	private float _health;
	public float Health { 
		get {
			return _health; 
		} 
		set{ 
			_health = value; 
			if (_health < 0.0f){
				gameObject.SetActive(false);
				_health         = 0.0f;
				this.enabled    = false;
				Color old       = _renderer.color;
				_renderer.color = new Color(old.r, old.g, old.b, 0.0f);
				OnDeath(this);
			}
		}
	}

	public bool IsDead(){ return Health < 0.0f; }
	
	public void PhaseOut(){ _isPhasedOut = true; }

	/*
	 * =========================================================================================
	 * Private Members
	 * =========================================================================================
	 */
	private SpriteRenderer _renderer;
	private bool _isPhasedOut = false;

	/*
	 * =========================================================================================
	 * Unity Interface Methods
	 * =========================================================================================
	 */
	void Awake(){
		Health = 1.0f;
		_renderer = GetComponent<SpriteRenderer>();
	}

	void Update(){
		if (!_isPhasedOut){
			Color old = _renderer.color;
			_renderer.color = new Color(old.r, old.g, old.b, healthToAlpha.Evaluate(Health));
		} else {
			Color old = _renderer.color;
			_renderer.color = new Color(old.r, old.g, old.b, old.a - phaseOutSpeed * Time.deltaTime);

			if (_renderer.color.a <= 0.0f){
				gameObject.SetActive(false);
				Destroy(this);
			}
		}
	}
}
