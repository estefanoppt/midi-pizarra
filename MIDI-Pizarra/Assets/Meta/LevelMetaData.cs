﻿  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelMetaData : GameMetaData
{
	// Pre Game-Loading
	public string nombre_nivel;         //":"Lonchera Básica Activity 1",
    	public string descripcion_nivel;    //":Captura alimentos saludable - Lento,
	
	// Post-Game Statistics
    	public string correctas;            //Selecciones correctas
    	public string incorrectas;          //

	public LevelMetaData(string id_registro) : base(id_registro) {
		tipo = "juego"; 
	}

}


