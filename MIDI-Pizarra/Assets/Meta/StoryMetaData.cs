﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;

[System.Serializable]
public class StoryMetaData : GameMetaData
{

	public string descripcion_historia;     
	public string duracion;                
	
	public StoryMetaData(string id_registro) : base (id_registro) {
		tipo = "historia";
	}
}
