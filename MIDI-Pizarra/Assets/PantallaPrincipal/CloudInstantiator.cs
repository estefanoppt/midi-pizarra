﻿using UnityEngine;

public class CloudInstantiator : MonoBehaviour {

	public float instantiationTime;
	public Transform spawnPoint;
	public Transform parent;
	public GameObject cloudObject;
	public CloudSlide initialCloud;

	void Awake(){
		InstantiateCloud();
		initialCloud.OnDestroyed += InstantiateCloud;
	}

	private void InstantiateCloud(){
		GameObject o = GameObject.Instantiate(cloudObject, spawnPoint.position, Quaternion.identity, parent);
		CloudSlide cloud = o.GetComponent<CloudSlide>();
		cloud.OnDestroyed += InstantiateCloud;
	}
}
