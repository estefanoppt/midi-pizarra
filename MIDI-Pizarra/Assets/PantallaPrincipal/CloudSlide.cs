﻿using UnityEngine;
using System;

public class CloudSlide : MonoBehaviour {

	public float speed    = 0.25f;
	public event Action OnDestroyed;

	private bool _hasBeenSeenOnce = false;
	void Update(){
		transform.position = new Vector3(
					transform.position.x + Time.deltaTime * speed,
					transform.position.y,
					transform.position.z
				);
	}

	void OnBecameVisible() => _hasBeenSeenOnce = true;

	void OnBecameInvisible(){
		if (!_hasBeenSeenOnce) return;
		OnDestroyed?.Invoke();
		GameObject.Destroy(gameObject);
	}

}
