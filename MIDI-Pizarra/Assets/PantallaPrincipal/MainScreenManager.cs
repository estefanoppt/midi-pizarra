﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class MainScreenManager : MonoBehaviour {

	public Button playButton;
	public string nextScene;

	public Button creditsButton;
	public string creditsScene;
	public GameObject returnToken;

	public Button hiddenConfig;
	public RectTransform configSelection;
	public int hiddenConfigHitCount = 10;
	private int _count = 0;

	public InputField schoolField;
	public InputField caseField;

	public Button configAccept;
	public Button configReject;

	public string mainInfoString = "mainInfo.data";

	void Awake(){

		playButton.onClick.AddListener( () => {
				GameObject.DontDestroyOnLoad(returnToken);
				UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
			});

		creditsButton.onClick.AddListener( () => {
				GameObject.DontDestroyOnLoad(returnToken);
				UnityEngine.SceneManagement.SceneManager.LoadScene(creditsScene);
			});

		hiddenConfig.onClick.AddListener( () => {
				_count++;
				if (_count >= hiddenConfigHitCount){
					_count = 0;
					configSelection.gameObject.SetActive(true);
				}	
			});

		configReject.onClick.AddListener( () => configSelection.gameObject.SetActive(false) );

		string appInfoPath = Application.persistentDataPath + "/" + mainInfoString; 
		configAccept.onClick.AddListener( () => {
					configSelection.gameObject.SetActive(false);
					GameStateManager.Instance.SaveGlobalSettings(new GlobalSettings(schoolField.text, caseField.text));

					// We save the values to disk.
					FileStream fs         = new FileStream(appInfoPath, FileMode.Create);
					BinaryFormatter b     = new BinaryFormatter();

					Tuple<string, string> schoolFields = new Tuple<string, string>(schoolField.text, caseField.text);
					b.Serialize(fs, schoolFields);

					fs.Close();

				});

		if (File.Exists(appInfoPath)){
			FileStream f = File.Open(appInfoPath, FileMode.Open);

			BinaryFormatter b = new BinaryFormatter();
			Tuple<string, string> schoolFields = (Tuple<string, string>) b.Deserialize(f);

			f.Close();

			schoolField.text = schoolFields.Item1;
			caseField.text   = schoolFields.Item2;
		}
	}

}
