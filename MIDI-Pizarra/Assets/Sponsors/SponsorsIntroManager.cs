﻿using UnityEngine;
using UnityEngine.Video;

public class SponsorsIntroManager : MonoBehaviour {
	public Animator uiController;
	public VideoPlayer player;
	public string nextSceneName = "MainScreen";

	public void Start(){
		player.Play();

		player.loopPointReached += (v) => uiController.SetBool("HasEnded", true);
	}

	public void Update(){
		if (uiController.GetNextAnimatorStateInfo(0).IsName("Off")){
			UnityEngine.SceneManagement.SceneManager.LoadScene(nextSceneName);
		}
	}
}
