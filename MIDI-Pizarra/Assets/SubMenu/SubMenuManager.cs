﻿using UnityEngine;
using UnityEngine.UI;

public class SubMenuManager : MonoBehaviour {
	public Button back;
	public string prevScene;

	public Button playStory;
	public string storySceneName;
	public AudioSource playStoryAudio;
	private bool _hasClickedStory = false;

	public Button playGame;
	public string gameSceneName;
	public AudioSource playGameAudio;
	private bool _hasClikedGame = false;

	public Button toCredits;
	public string creditsScene;

	void Awake(){
		back.onClick.AddListener( () => UnityEngine.SceneManagement.SceneManager.LoadScene(prevScene) );
		playStory.onClick.AddListener( () => {
					playStoryAudio.Play();
					_hasClickedStory = true;
				});

		playGame.onClick.AddListener( () => {
					playGameAudio.Play();
					_hasClikedGame = true;
				});

		toCredits.onClick.AddListener( () => {
				UnityEngine.SceneManagement.SceneManager.LoadScene(creditsScene);
			});
	}

	void Update(){
		if (_hasClickedStory && !playStoryAudio.isPlaying){
			UnityEngine.SceneManagement.SceneManager.LoadScene(storySceneName);
		}

		if (_hasClikedGame && !playGameAudio.isPlaying){
			UnityEngine.SceneManagement.SceneManager.LoadScene(gameSceneName);
		}
	}
}
